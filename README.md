# Informations concernant la chaine Twitch [LeifBeth](https://twitch.tv/leifbeth)
Une chaine dédiée principalement au développement web et à la veille technologique, retrouvez ici toutes les informations utiles du live ;)

## Sommaire
* [Qui suis-je ?](#qui-suis-je-)
* [Le Planning](#le-planning)
* [Discord](#discord)
* [Mon Matos](#mon-matos)
* [Mes Projets](#mes-projets)

## Qui suis-je ?
André LECLERCQ, 34 ans, développeur web depuis 3 ans, autodidacte depuis plus de 15 ans, je suis passionné par mon boulot et mon jour préféré c'est le Lundi. Cette chaine n'a pas pour but de "perçer", juste de partager avec les potes ce que je fais et s'entre-aider au mieux :)
> [retour au sommaire](#sommaire)

## Le Planning
Celui ci est suceptible de changer assez régulièrement, étant en phase de grands changements professionnels rien n'est sûr, pour voir les projets en cours consultez [Mes Projets](#mes-projets).
* Du Lundi au Vendredi [13h00 - 15h00] | **Dev Web & Chill** - Développement Web ou apprentissage d'un nouveau language, ce créneau est prévu pour la conception Software ;)
* Samedi [8h00 - 12h00] | **Electronique et Bricolage** - Canniblalisation de laptop, RaspberryPI, Arduino... le samedi matin est dédié principalement au bricolage autour de l'éléctronique.
> [retour au sommaire](#sommaire)

## Discord
J'ai créé un discord pas vraiment pour la chaine mais pour discuter dans un coin en mode peinard, c'est la que je partage tout ce qui me passe par la tête, dev, arduino, crypto.... Vous pouvez le rejoindre sans problème [Rejoindre le Discord](discord.gg/PPAMxgY)

Je pose également le discord de copains streamer Francophone de la section Science & Tech de twitch, [Rejoindre le Discord de From Scratch](https://discord.gg/45f3kWUUsf)
> [retour au sommaire](#sommaire)

## Mon Matos
Il faut savoir une chose, je suis actuellement dans une mouvance d'économie pécunière et energetique, j'espère réussir à prouver que nous n'avons pas besoin d'une bête de PC ou d'une connexion de malade pour faire des petits stream de dev ;) (N'oubliez pas de regarder le stream et les VOD en 720p c'est bon pour la planète :D ) 

### PC Principale (stream)
#### Hardware
* Intel NUCI3BEH
* Processeur i3 3Ghz
* DDR4 8Go
* SSD m2 240Go

#### Software
* OS : Linux Debian 11 (bullseye)
* Browser : Vivaldi
* IDE : Intellij IDEA / VSCode
* Stream : OBS Studio

Pour le moment c'est tout mais j'ai prévu d'installer un raspberryPi 3 très prochainement, ainsi que de recycler des écrans de laptop pour me faire un dual screen.
> [retour au sommaire](#sommaire)

## Mes Projets
### Projets en Cours
- (13h-15h en live) Apprentissage du RUST from scratch
- (Samedi matin en live) Cannibalisation d'un vieux Laptop, dans un premier temps on va récupérer l'écran pour m'en servir de second moniteur pour mon NUC8. D'autres pièces seront récupérer par la suite, webcam, clavier, puce WIFI, disque dur...

### Bucket List 2021
Pour l'année 2021 j'ai prévu une petite "bucket list" que voici, vous pourrez suivre sont avancé en live, sur discord et je ferai la mises à jour ici également ;)
- [ ] Migrer et travailler sous Linux.
- [ ] Apprendre le RUST.
- [ ] Refacto JORD mon projet de Single Page Application en JS/NodeJS/MongoDB.
- [ ] Installer un serveur sous RaspberryPi 3.
- [ ] Recycler deux écrans de laptop cassé pour en fare des écrans autonome.
- [ ] Organiser mon espace de travail en remote de manière optimale.

Je ne me priverai pas d'ajouter d'autres petites choses au cours de l'année ;)
> [retour au sommaire](#sommaire)
